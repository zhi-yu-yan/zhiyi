# 知意操作系统

#### 介绍
知意操作系统（基于sanos）--引导程序使用NASM,其他部分使用C语言（tcc）开发。可以作为服务器运行,能运行java4及其以下的项目。本操作系统完全自举，不需要任何的外部依赖。所有的汇编器源码，编译器源码，库源码，操作系统源码全部包含在了本项目内。完全实现了自己编译自己向前升级。是嵌入式系统开发不可多得的源码。本操作系统源码比linux早期源码要容易上手很多，另外套上GUI就是一个小型的windows操作系统。

#### 操作系统运行截图

![输入图片说明](bin/%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F%E6%88%AA%E5%9B%BE/01.png)

#### 操作系统功能

ls： 显示当前目录下的所有文件和文件夹
reboot:重新启动系统
more /proc/version:显示操作系统当前版本信息
more /proc/memmap：BIOS内存映射信息


1. more /proc/memusage：每个分配类型的内存使用情况
1. 
1. more /proc/memstat：内存统计摘要
1. 
1. more /proc/physmem：物理内存映射
1. 
1. more /proc/pdir：页面目录信息
1. 
1. more /proc/virtmem：虚拟内存范围
1. 
1. more /proc/kmem：内核内存
1. 
1. more /proc/kmodmem：内核模块内存
1. 
1. more /proc/kheap：内核堆统计信息
1. 
1. more /proc/vmem：用户虚拟内存映射
1. 
1. more /proc/cpu：cpu信息
1. 
1. more /proc/traps：陷阱/中断统计信息
1. 
1. more /proc/uptime：自上次启动以来的时间
1. 
1. more /proc/loadavg：CPU平均负载
1. 
1. more /proc/threads：线程信息
1. 
1. more /proc/dpcs：DPC统计信息
1. 
1. more /proc/handles：手柄表
1. 
1. more /proc/syscalls：系统调用统计信息
1. 
1. more /proc/files：打开文件
1. 
1. more /proc/bufpools：缓冲池
1. 
1. more /proc/bufstats：缓冲池统计信息
1. 
1. more /proc/kmods：内核模块
1. 
1. more /proc/umods：用户模块
1. 
1. more /proc/units：PCI和PnP枚举中的已安装设备
1. 
1. more /proc/devices：已安装的设备
1. 
1. more /proc/screen：屏幕缓冲区
1. 
1. more /proc/apm：高级电源管理信息
1. 
1. more /proc/pbufs：网络数据包缓冲区
1. 
1. more /proc/netstat：网络数据包统计
1. 
1. more /proc/netif：网络接口
1. 
1. more /proc/arp：arp表
1. 
1. more /proc/udpstat：UDP套接字
1. 
1. more /proc/rawstat：RAW插座
1. 
1. more /proc/dhcpstat：DHCP信息
1. 
1. more /proc/tcpstat：TCP套接字
1. 
1. ...操作系统根目录 cd poc 然后ls便可以看到所有的more /proc/功能
1. cd:目录跳转
1. $ cat hello.c :直接在控制台打开文件源码


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
