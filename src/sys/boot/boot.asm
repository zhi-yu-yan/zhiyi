;****************************************************************************************************
; 操作系统启动过程步骤1：BIOS
; 描述：操作系统启动过程步骤2：引导扇区加载引导加载程序（本程序源码存储在启动盘的:0号磁头对应的0磁道1扇区，共512字节）
; www.880.xin
;****************************************************************************************************


OSLDRSEG equ 0x9000

ORG 0x7c00    ;指定程序的装载地址
 ;-----------------------------------------------------1.初始bootstap代码的入口点---------------------------------------------------------

        jmp short start
        nop
        nop

        db      'ZHIYI-OS'    ;启动区的名称, 必须 8 个字节
ldrsize dw     32         ;每磁道扇区数
ldrstrt dd     8

start:  ;设置初始环境
        mov     ax, cs
        mov     ds, ax
        mov     [bootdrv], dl   ;保存启动驱动器。bios载入引导程序后会自动把引导盘的驱动器号存入dl寄存器中
        mov     si, btmsg
        call    printstr
;--------------------------------------------------------1.读磁盘------------------------------------------------------
;读扇区
;		mov ah, 02h
;		mov al, 要读的扇区的个数
;		mov ch, 磁道号 （柱面号）
;		mov cl, 起始扇区号
;		mov dh, 磁头号
;		mov dl, 驱动器号(0表示A盘)             ;软盘（00H~7FH），硬盘（80H~0FFH）
;		mov bx, 数据写入内存的地址，完整寻址是es:bx
;		int 13h ;出口参数：CF＝0——操作成功，CH=柱面数的低8位，CL的7-6位=柱面数的高2位，CL的5-0位=扇区数，DH=磁头数，DL=驱动器数，ES:DI＝磁盘驱动器参数表地址

readldr:
        ; 获取启动驱动器几何结构
        mov     dl, [bootdrv]  ;要读取的磁盘（0表示A软盘）
        mov     ah, 08h    ;获取驱动器参数
        xor     di, di   ;DI寄存器数据清零
        mov     es, di
        int     0x13     ;读取磁盘扇区到指定的内存

        and     cl, 0x3F
        mov     byte [sectors], cl
        inc     dh                   ; dh=磁头号 相当于：dh=dh+1
        mov     byte [heads], dh
        ;从引导驱动器加载操作系统加载程序
loadnext:
        xor     eax, eax
        mov     ax, [sectno]        ; eax=操作系统加载程序扇区号。
        mov     bx, ax
        shl     bx, 5               ; 每个扇区512/16段.使bx左移5位（010100000）
        add     bx, OSLDRSEG        ; bx=bx+0x9000,（bx=0x90a0）
        mov     es, bx              ; es=下一个os加载器扇区的段
        mov     di, [ldrsize]
        sub     di, ax              ; di=di-ax;di=剩余读取的os加载器扇区数
        mov     cx, [sectno]        ; 确保我们没有跨越64KB的边界
        and     cx, 0x7F
        neg     cx                  ;求补指令，对操作数执行求补运算：用零减去操作数，然后结果返回操作数。求补运算也可以表达成：将操作数按位取反后加1。
        add     cx, 0x80
        cmp     cx, di
        jg      loadnext1           ;若 左操作数>右操作数  则跳转

        mov     di, cx
loadnext1:
        mov     ebx, [ldrstrt]      ; eax=下一个os加载器扇区的LBA（逻辑寻址）
        add     eax, ebx
        call    readsectors

        mov     ax, [sectno]        ; 更新下一个os加载器扇区以读取
        add     ax, di
        mov     [sectno], ax
        cmp     ax, [ldrsize]
        jnz     loadnext            ;非零跳转

        ; 在os加载器中调用实模式入口点
        mov     ax, OSLDRSEG
        mov     ds, ax
        add     ax, [0x16]          ; cs = 9016H
        push    ax
        push    word [0x14]         ; ip = 14H
        mov     dl, [cs:bootdrv]    ; 启动驱动器
        xor     ebx, ebx            ; RAM磁盘映像
        retf                        ;call far的返回
;------------------------------------------------------2.读扇区-子程序--------------------------------------------------------
readsectors: ;从启动驱动器读扇区，（输入：eax=逻辑寻址，di=最大扇区数，es=缓冲区段），（输出：di=已读扇区）
        ;将LBA转换为CHS(chs的三个参数：柱面共10比特，最大1023；磁头8比特，最大255；扇区6比特，最大63。因此CHS寻址的最大范围也才7.387GB。CHS只适用于内外盘扇区数相同的老式磁盘，现代磁盘由于内外圈扇区数不同，采用的是线性的扇区编号寻址方式，也就是只通过扇区号来定位扇区，但是它依然可以通过地址翻译和CHS之间相互换算。)
        cdq                         ; cdq把EDX的所有位都设成EAX最高位的值 。edx = 0
        movzx   ebx, word [sectors] ;小数复制到大数，高位用0填补。
        div     ebx                 ; eax = 磁道, edx = 扇区 - 1
        mov     cx, dx              ; cl = 扇区 - 1, ch = 0
        inc     cx                  ; cl = 扇区数
        xor     dx, dx
        mov     bl, [heads]
        div     ebx
        mov     dh, dl              ; head
        mov     dl, [bootdrv]       ; 启动驱动器
        xchg    ch, al              ; 交换ch和al的内容。 ch = 柱面号的低8位, al = 0
        shr     ax, 2               ; al[6:7] = 柱面的高两位, ah = 0
        or      cl, al              ; cx = 柱面和扇区
        ; 确定要读取的扇区数
        mov     al, cl
        mov     ah, 0
        add     ax, di              ; ax = xfer的最后一个扇区
        cmp     ax, [sectors]
        jbe     readall

        mov     ax, [sectors]       ; 读到磁道末尾
        inc     ax
        sub     al, cl
        push    ax
        jmp     read
readall:
        mov     ax, di              ; 我们可以读取所有扇区
        push    ax
        jmp     read
readagain:
        pusha                       ;PUSHA: 依次把AX,CX,DX,BX,SP,BP,SI,DI等的值压入栈中
        mov     al, '#'             ; 打印#以标记错误
        call printchar
        mov     ax, 0               ; 重置磁盘系统
        mov     dx, 0               ; dl=驱动器号（0表示A软盘）
        int     0x13
        popa                        ;POPA: 把栈中的值依次弹到DI,SI,BP,SP,BX,DX,CX,AX等寄存器中
read:    ;读磁盘(ah=2h,al=扇区数,ch=柱面,cl=起始扇区,dh=磁头,dl=驱动器,es:bx=缓冲区地址),返回，成功CF=0，AH=00h,AL=传输的扇区数
        mov     ah, 2               ; 如果ah=02h（读磁盘） 则al=要读取扇区的个数
        xor     bx, bx              ; bx数据清零
        int     0x13                ; BIOS读磁盘

        jc      readagain           ; 如进位标志为1则跳转到readagain
        pop     di
        ret
 ;-----------------------------------------------------2.打印-子程序---------------------------------------------------------
printstr:
        push    ax
        cld ;cld使DF=0(内存地址向高地址增加),std使DF=1(向低地址减小)。 这两个指令用于串操作指令中。 通过执行cld或std指令可以控制方向标志DF。
print:
        mov     al, [si]   ; AL＝字符
        cmp     al, 0
        je      printdone  ;当ZF等于0时跳转
        call    printchar
        inc     si          ;相当于：si=si+1
        jmp     print
printchar:
        mov     ah, 0x0e ; 在Teletype（虚拟控制台，串口以及伪终端设备组成的终端设备）模式下显示一个文字。
        int     0x10     ;调用显卡bios 中断, 每次中断都会将al寄存器中的值打印在屏幕
        ret
printdone:
        pop     ax
        ret
;---------------------------------------------------------变量-----------------------------------------------------
sectno  dw     0
sectors dw     0    ;扇区数
heads   dw     0
bootdrv db     0    ;启动驱动器

ceshi db 'a'

btmsg   db      'Booting... bootstrap', 0
        times   510-($-$$) db 0 ; 填充剩下的空间，使生成的二进制代码恰好为512字节.$-$$表示本行距离程序开始处的相对距离。
        dw      0xAA55
