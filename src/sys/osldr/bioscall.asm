;
; bioscall.asm
;
; 保护模式BIOS调用
; 

        SECTION .text

        global  prot2real
        global  _prot2real
        global  real2prot
        global  _real2prot
        global  bios_print_string
        global  _bios_print_string
        global  bios_get_drive_params
        global  _bios_get_drive_params
        global  bios_read_disk
        global  _bios_read_disk
        global vesa_get_info
        global _vesa_get_info
        global vesa_get_mode_info
        global _vesa_get_mode_info
        global vesa_set_mode
        global _vesa_set_mode

; OS加载器基地址
OSLDRSEG    equ 0x9000
OSLDRBASE   equ (OSLDRSEG * 16)

; 实模式和保护模式下数据和代码的段选择器
PROT_CSEG   equ 0x08
PROT_DSEG   equ 0x10
REAL_CSEG   equ 0x18
REAL_DSEG   equ 0x20

; 实模式堆栈的位置
REAL_STACK  equ (0x2000 - 0x10)

; 将线性地址转换为osldr段相对地址
%define LOCAL(addr) (addr - OSLDRBASE)

;
; 从保护模式切换到真实模式
;

prot2real:
_prot2real:
        BITS    32
        ; 切换模式时无中断
        cli

        ; 保存保护模式堆栈和基指针
        mov     eax, esp
        mov     [prot_esp], eax
        mov     eax, ebp
        mov     [prot_ebp], eax
        
        ; 保存中断描述符
        sidt    [prot_idt]
        sgdt    [prot_gdt]

        ; 将返回地址存储在实模式堆栈中
        mov     eax,  [esp]
        mov     [REAL_STACK], eax

        ; 设置实模式堆栈
        mov     eax, REAL_STACK
        mov     esp, eax
        mov     ebp, eax

        ; 设置段寄存器
        mov     ax, REAL_DSEG
        mov     ds, ax
        mov     es, ax
        mov     fs, ax
        mov     gs, ax
        mov     ss, ax

        ; 转换为16位段
        jmp     REAL_CSEG:(LOCAL(start16))

start16:
        BITS    16

        ; 通过清除CR0的PE位退出保护模式
        mov     eax, cr0
        and     eax, ~1
        mov     cr0, eax

        ; 重新加载代码段寄存器并清除预取
        jmp     dword OSLDRSEG:(LOCAL(realmode))

realmode:

        ; 设置实模式段寄存器
        mov     ax, OSLDRSEG
        mov     ds, ax
        mov     es, ax
        mov     fs, ax
        mov     gs, ax
        xor     ax, ax
        mov     ss, ax
        
        ; 加载实模式IDT
        a32 lidt [LOCAL(real_idt)]

        ; 返回实模式堆栈
        sti
        ret

;
; 从真实模式切换到保护模式
;

real2prot:
_real2prot:
        BITS    16
        ; 屏蔽中断
        cli
        
        ; 恢复保护的模式描述符
        mov     ax, OSLDRSEG
        mov     ds, ax
        a32 lidt    [LOCAL(prot_idt)]
        a32 lgdt    [LOCAL(prot_gdt)]

        ; 启用保护模式
        mov     eax, cr0
        or      eax, 1
        mov     cr0, eax

        ; 设置代码段并清除预取
        jmp     dword PROT_CSEG:protmode

protmode:
        BITS    32
        ; 为保护模式设置段寄存器的其余部分
        mov     ax, PROT_DSEG
        mov     ds, ax
        mov     es, ax
        mov     ss, ax
        mov     fs, ax
        mov     gs, ax

        ; 将返回地址存储在实模式堆栈中
        mov     eax, [esp]
        mov     [REAL_STACK], eax

        ; 恢复受保护模式堆栈
        mov     eax, [prot_esp]
        mov     esp, eax
        mov     eax, [prot_ebp]
        mov     ebp, eax

        ; 将返回地址放入保护模式堆栈
        mov     eax, [REAL_STACK]
        mov     [esp], eax

        ; 返回保护模式堆栈
        xor     eax, eax
        ret
;
; 在屏幕上打印字符串
;
; void bios_print_string(char *str);
;

bios_print_string:
_bios_print_string:
        push ebp
        mov  ebp, esp

        ; 保存寄存器
        push    ebx
        push    esi
        push    edi

        ; 获取字符串地址
        mov     esi, [ebp + 8]

        ; 进入实模式
        call    _prot2real
        BITS     16

        ; 输出所有字符到字符串
nextchar:
        mov     al, [si]
        cmp     al, 0
        je      printdone

        mov     ah, 0x0e
        int     0x10
        
        cmp     al, 10
        jne     notnl

        ; 换行符的输出cr/nl
        mov     al, 13
        mov     ah, 0x0e
        int     0x10
        
notnl:

        inc     si
        jmp     nextchar
printdone:
                
        ; 返回保护模式
        call    _real2prot
        BITS    32

        ; 恢复寄存器
        pop     edi
        pop     esi
        pop     ebx

        ; 返回
        pop    ebp
        ret

;
; 获取驱动参数
;
; int bios_get_drive_params(int drive, unsigned int *cyls, unsigned int *heads, unsigned int *sects);
;

bios_get_drive_params:
_bios_get_drive_params:
        push ebp
        mov  ebp, esp

        ; 保存寄存器
        push    ebx
        push    esi
        push    edi

        ; 获取驱动器数
        mov     edx, [ebp + 8]

        ; 进入实模式
        call    _prot2real
        BITS     16

        ; 调用BIOS int13h/ah=08h获取驱动器参数
        mov     ah, 0x08
        xor     di, di
        mov     es, di
        int     0x13
        mov     bl, ah
                
        ; 返回保护模式
        call    _real2prot
        BITS    32
        
        ; 计算柱面数
        xor     eax, eax
        mov     al, ch
        mov     ah, cl
        shr     ah, 6
        inc     ax
        mov     edi, [ebp + 12]
        mov     [edi], eax

        ; 计算heads数
        xor     eax, eax
        mov     al, dh
        inc     ax
        mov     edi, [ebp + 16]
        mov     [edi], eax

        ; 计算扇区数
        xor     eax, eax
        mov     al, cl
        and     al, 0x3f
        mov     edi, [ebp + 20]
        mov     [edi], eax

        ; 返回状态
        xor     eax, eax
        mov     al, bl

        ; 恢复寄存器
        pop     edi
        pop     esi
        pop     ebx

        ; 返回
        pop     ebp
        ret

;
; 从磁盘读取
;
; int bios_read_disk(int drive, int cyl, int head, int sect, int nsect, void *buffer);
;

bios_read_disk:
_bios_read_disk:
        push ebp
        mov  ebp, esp

        ; Save register
        push    ebx
        push    esi
        push    edi

        ; Get cylinder, head, and sector, number of sectors, and buffer
        mov     al, [ebp + 24]          ; number of sectors
        mov     ch, [ebp + 12]          ; cylinders (lo)
        mov     cl, [ebp + 13]          ; cylinders (hi)
        shl     cl, 6
        or      cl, [ebp + 20]          ; sector number
        mov     dh, [ebp + 16]          ; head number
        mov     dl, [ebp + 8]           ; drive number
        mov     ebx, [ebp + 28]         ; data buffer
        sub     ebx, OSLDRBASE

        ; Save ax which is not preserved in prot->real transition
        mov     si, ax

        ; Enter real mode
        call    _prot2real
        BITS     16

        ; Call BIOS int13h/ah=02h to read disk
        mov     ax, si
        mov     ah, 0x02
        int     0x13
        mov     bl, ah

        ; Return to protected mode
        call    _real2prot
        BITS    32

        ; Return status
        xor     eax, eax
        mov     al, bl

        ; Restore registers
        pop     edi
        pop     esi
        pop     ebx

        ; Return
        pop    ebp

        ret

;
; VBE功能调用。获取 VESA 信息.返回一个VbeInfoBlock结构体即vesa_info
;
; int vesa_get_info(struct vesa_info *info);
;

vesa_get_info:
_vesa_get_info:
        push ebp
        mov  ebp, esp

        ; 保存寄存器
        push    ebx
        push    esi
        push    edi

        ; 获取信息缓冲区地址
        mov     edi, [ebp + 8]
        sub     edi, OSLDRBASE

        ; 进入实模式
        call    _prot2real
        BITS     16

        ; 调用 VESA
        mov     ax, OSLDRSEG
        mov     es, ax       ;es:di = 指向存放VBEInfoBlock结构体的缓冲区指针
        mov     ax, 0x4f00
        int     0x10
        mov     bx, ax

        ; 返回保护模式
        call    _real2prot
        BITS    32

        ; 返回状态
        xor     eax, eax
        mov     ax, bx      ;此函数返回一个vesa_info结构体

        ; 恢复寄存器
        pop     edi
        pop     esi
        pop     ebx

        ; 返回
        pop    ebp

        ret

;
; 获取 VESA 模式信息
;
; int vesa_get_mode_info(int mode, struct vesa_mode_info *info);
;

vesa_get_mode_info:
_vesa_get_mode_info:
        push ebp
        mov  ebp, esp

        ; Save register
        push    ebx
        push    esi
        push    edi

        ; 获取模式和信息缓冲区地址
        mov     ecx, [ebp + 8]
        mov     edi, [ebp + 12]
        sub     edi, OSLDRBASE

        ; 进入实模式
        call    _prot2real
        BITS     16

        ; 调用 VESA
        mov     ax, OSLDRSEG
        mov     es, ax       ;es:di  = 指向VBE特定模式信息块的指针
        mov     ax, 0x4f01   ;返回VBE模式信息
        int     0x10
        mov     bx, ax

        ; 返回保护模式
        call    _real2prot
        BITS    32

        ; 返回状态
        xor     eax, eax
        mov     ax, bx      ;返回一个vesa_mode_info结构体

        ; 恢复寄存器
        pop     edi
        pop     esi
        pop     ebx

        ; 返回
        pop    ebp

        ret

;
; 设置VESA图形模式
;
; int vesa_set_mode(int mode);
;

vesa_set_mode:
_vesa_set_mode:
        push ebp
        mov  ebp, esp

        ; 保存寄存器
        push    ebx
        push    esi
        push    edi

        ; 获取模式
        mov     ebx, [ebp + 8]  ;需要设置的模式

        ; 进入实模式
        call    _prot2real
        BITS     16

        ; Call VESA
        mov     ax, 0x4f02 ;设置VBE模式
        int     0x10
        mov     bx, ax

        ; 返回保护模式
        call    _real2prot
        BITS    32

        ; 返回状态
        xor     eax, eax
        mov     ax, bx

        ; 回复寄存器
        pop     edi
        pop     esi
        pop     ebx

        ; 返回
        pop    ebp

        ret

; 存储保护模式堆栈指针和描述符表
prot_esp:   dd 0
prot_ebp:   dd 0
prot_idt:   dw 0, 0, 0
prot_gdt:   dw 0, 0, 0

; 实模式 IDT for BIOS
real_idt:   dw 0x3ff   ; 256 entries, 4b each = 1K
            dd 0       ; Real mode IVT @ 0x0000
