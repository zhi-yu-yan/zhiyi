/****************************************************************************************************
 * 描述：引导加载程序：设置内存并加载内核（osldr.dll）
 * www.880.xin
 ****************************************************************************************************/

#include <os.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>

#include <os/pdir.h>
#include <os/tss.h>
#include <os/seg.h>
#include <os/syspage.h>
#include <os/timer.h>
#include <os/fpu.h>
#include <os/object.h>
#include <os/sched.h>
#include <os/dfs.h>
#include <os/dev.h>
#include <os/vga.h>

#define VIDEO_BASE           0xB8000  //显存物理地址范围：文本模式： 0xB8000-0xBFFFF 图形模式：0xA0000-0xBFFFF
#define HEAP_START           (1024 * 1024)

unsigned long mem_end;          // 内存大小
char *heap;                     // 堆指针指向内存中的下一个可用页
pte_t *pdir;                    // 页面目录
struct syspage *syspage;        // 带有tss和gdt的系统页面
pte_t *syspagetable;            // 系统页表
struct tcb *InitThreadControlBlock;            // 内核的初始线程控制块
unsigned long kernel_entry;     // 内核入口点的虚拟地址
unsigned short tssval;          // TSS选择器值
unsigned short ldtnull;         // LDT零点选择器
struct selector gdtsel;         // GDT选择器
struct selector idtsel;         // IDT选择器
int bootdrive;                  // 引导驱动器
int bootpart;                   // 启动分区
char *kernel_options;           // 内核选项
char *initrd;                   // 堆中的初始RAM磁盘位置
int initrd_size;                // 初始RAM磁盘大小（字节）
int bootdev_cyls;               // 启动设备柱面
int bootdev_heads;              // 引导设备头
int bootdev_sects;              // 每个磁道的引导设备扇区
int x_resolution, y_resolution, bpp;            // 视频分辨率
char scratch[4096];             // 划痕缓冲区

int vsprintf(char *buf, const char *fmt, va_list args);
void bios_print_string(char *str);
int bios_get_drive_params(int drive, int *cyls, int *heads, int *sects);
int bios_read_disk(int drive, int cyl, int head, int sect, int nsect, void *buffer);
int vesa_get_info(struct vesa_info *info);
int vesa_get_mode_info(int mode, struct vesa_mode_info *info);
int vesa_set_mode(int mode);
int unzip(void *src, unsigned long srclen, void *dst, unsigned long dstlen, char *heap, int heapsize);
void load_kernel();

void kprintf(const char *fmt,...) {
  va_list args;
  char buffer[1024];

  va_start(args, fmt);
  vsprintf(buffer, fmt, args);
  va_end(args);

  bios_print_string(buffer);
}

void panic(char *msg) {
  kprintf("panic: %s\n", msg);
  while (1);
}

void init_biosdisk() {
  int status;
  
  status = bios_get_drive_params(bootdrive, &bootdev_cyls, &bootdev_heads, &bootdev_sects);
  if (status != 0)
  {
	  panic("无法初始化启动设备");
  }
}

int biosdisk_read(void *buffer, size_t count, blkno_t blkno) {
  char *buf = buffer;
  int sects = count / SECTORSIZE;
  int left = sects;
  while (left > 0) {
    int nsect, status;
    
    // 计算CHS地址
    int sect = blkno % bootdev_sects + 1;
    int track = blkno / bootdev_sects;
    int head = track % bootdev_heads;
    int cyl = track / bootdev_heads;
    if (cyl >= bootdev_cyls) return -ERANGE;
    
    // 计算要读取的扇区数
    nsect = left;
    if (nsect > sizeof(scratch) / SECTORSIZE) nsect = sizeof(scratch) / SECTORSIZE;
    if (nsect > 0x7f) nsect = 0x7f;
    if ((sect - 1) + nsect > bootdev_sects) nsect = bootdev_sects - (sect - 1);

    // 将磁盘中的扇区读取到内存不足的临时缓冲区中。
    status = bios_read_disk(bootdrive, cyl, head, sect, nsect, scratch);
    if (status != 0) {
      kprintf("biosdisk: read error %d\n", status);
      return -EIO;
    }

    // 将数据移动到高内存
    memcpy(buf, scratch, nsect * SECTORSIZE);
    
    // 准备下一次阅读
    blkno += nsect;
    buf += nsect * SECTORSIZE;
    left -= nsect;
  }

  return count;
}

int bootrd_read(void *buffer, size_t count, blkno_t blkno) {
  memcpy(buffer, initrd + blkno * SECTORSIZE, count);
  return count;
}
/* 启动扇区读取
 * */
int boot_read(void *buffer, size_t count, blkno_t blkno) {
  if ((bootdrive & 0xF0) == 0xF0) {
    return bootrd_read(buffer, count, blkno);
  } else {
    return biosdisk_read(buffer, count, blkno);
  }
}
/* 新建堆
 * */
char *new_heap(int numpages) {
	// 检查内存是否不足
	if ((unsigned long) heap + numpages * PAGESIZE >= mem_end)
	{
		panic("out of memory");
	}
	return heap;
}

char *alloc_heap(int numpages)
{
  char *p = new_heap(numpages);
  memset(p, 0, numpages * PAGESIZE);
  // 更新堆指针
  heap += numpages * PAGESIZE;

  return p;
}

//内存大小
unsigned long memsize()
{
  volatile unsigned long *mem;
  unsigned long addr;
  unsigned long value;
  unsigned long cr0save;
  unsigned long cr0new;

  addr = 1024 * 1024;// 以1MB开始
  __asm { mov eax, cr0 };// 保存CR0的副本
  __asm { mov [cr0save], eax };
  __asm { wbinvd };// 使缓存无效（写回并使缓存无效）。BIOS入口点以WBINVD指令开头。其功能是使缓存无效。
  cr0new = cr0save | 0x00000001 | 0x40000000 | 0x20000000;// 仅使用PE/CD/NW插入cr0（缓存禁用（486+），无写回（486+），32位模式（386+））
  __asm { mov eax, [cr0new] };
  __asm { mov cr0, eax };

  // 每MB的查看
  while (addr < 0xFFF00000)
  {
    addr += 1024 * 1024;
    mem= (unsigned long *) addr;
    value = *mem;
    *mem = 0x55AA55AA;
    if (*mem != 0x55AA55AA)
    {
    	break;
    }
    *mem = 0xAA55AA55;
    if(*mem != 0xAA55AA55)
    {
    	break;
    }

    *mem = value;
  }
  // 恢复
  __asm { mov eax, [cr0save] };
  __asm { mov cr0, eax };
  return addr;
}

/*设置内存大小
 * memmap：6
 * */
void setup_memory(struct memmap *memmap)
{

  int i;
  if (memmap->count != 0)
  {
    // 从BIOS内存映射中确定最大可用RAM地址
    mem_end = 0;
    for (i = 0; i < memmap->count; i++)
    {
      if (memmap->entry[i].type == MEMORY_TYPE_RAM)
      {
        mem_end = (unsigned long) (memmap->entry[i].addr + memmap->entry[i].size);
      }
    }
  } else
  {
    // 没有BIOS内存映射，探测内存并创建一个带有640K:1Mb的简单映射
    mem_end = memsize();
    memmap->count = 3;

    memmap->entry[0].addr = 0;
    memmap->entry[0].size = 0xA0000;
    memmap->entry[0].type = MEMORY_TYPE_RAM;

    memmap->entry[1].addr = 0xA0000;
    memmap->entry[1].size = 0x60000;
    memmap->entry[1].type = MEMORY_TYPE_RESERVED;

    memmap->entry[2].addr = 0x100000;
    memmap->entry[2].size = mem_end - 0x100000;
    memmap->entry[2].type = MEMORY_TYPE_RAM;
  }
}

/* 功能：设置页表。
 * 描述：页表：是一种特殊的数据结构，记录着页面和页框的对应关系。（映射表）页表的作用：是内存非连续分区分配的基础，实现从逻辑地址转化成物理地址。
 * */
void setup_page_tables()
{
  pte_t *pt;
  int i;
  /*3.为页面目录分配页面*/
  pdir = (pte_t *) alloc_heap(1);  // pdir = 0x100000
  /*4.生成用于访问页表的递归条目。*/
  pdir[PDEIDX(PAGE_TABLE_BASE)] = (unsigned long) pdir | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE; //pdir[PDEIDX(PAGE_TABLE_BASE)] = 0x100003
  /*5.分配系统页面。*/
  syspage = (struct syspage *) alloc_heap(1);       //syspage = 0x101000
  /*6.分配初始线程控制块 .*/
  InitThreadControlBlock = (struct tcb *) alloc_heap(PAGES_PER_TCB);// InitThreadControlBlock = 0x102000
  /*7.分配系统页面目录页面 . */
  syspagetable = (pte_t *) alloc_heap(1);            //syspagetable = 0x104000
  /*8.映射系统页面、页面目录和视频缓冲区 . */
  pdir[PDEIDX(SYS_PAGE_BASE)] = (unsigned long) syspagetable | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;//pdir[PDEIDX(SYS_PAGE_BASE)]= 0x104003
  syspagetable[PTEIDX(SYSPAGE_ADDRESS)] = (unsigned long) syspage | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;//syspagetable[PTEIDX(SYSPAGE_ADDRESS)]=0x101003
  syspagetable[PTEIDX(PAGEDIR_ADDRESS)] = (unsigned long) pdir | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;//syspagetable[PTEIDX(VIDEO_BASE_ADDRESS)]=0x100003
  syspagetable[PTEIDX(VIDEO_BASE_ADDRESS)] = VIDEO_BASE | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;//syspagetable[PTEIDX(VIDEO_BASE_ADDRESS)] = 0xb8003
  /*映射初始TCB(线程控制块)*/
  for (i = 0; i < PAGES_PER_TCB; i++)
  {
	  /* syspagetable[PTEIDX(INITTCB_ADDRESS) + 0] = 0x102003
	   * syspagetable[PTEIDX(INITTCB_ADDRESS) + 1] = 0x103003
	   * */
    syspagetable[PTEIDX(INITTCB_ADDRESS) + i] = ((unsigned long) InitThreadControlBlock + i * PAGESIZE) | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;
  }
  /* 9.将第一个4MB临时映射到物理内存*/
  pt = (pte_t *) alloc_heap(1);//pt = 0x105000
  pdir[0] = (unsigned long) pt | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE | PAGE_TABLE_USER; // pdir[0] = 0x105007
  for (i = 0; i < PTES_PER_PAGE; i++)
  {
	  /*
	   * pt[0] =    0x3
	   * pt[1] =    0x1003
	   * pt[2] =    0x2003
	   * .
	   * pt[16] =   0x10003
	   * .
	   * pt[1022] = 0x3fe003
	   * pt[1023] = 0x3ff003
	   * */
	  pt[i] = (i * PAGESIZE) | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;
  }
}
/* 设置描述符
 * */
void setup_descriptors() {
  struct syspage *syspage;
  struct TaskStateSegment *TaskStateSegment;

  // 获取syspage虚拟地址
  syspage = (struct syspage *) SYSPAGE_ADDRESS;//syspage=0x90400000

  // 初始化tss
  TaskStateSegment = &syspage->TaskStateSegment;
  TaskStateSegment->cr3 = (unsigned long) pdir;
  TaskStateSegment->eip = kernel_entry;
  TaskStateSegment->cs = SEL_KTEXT;
  TaskStateSegment->ss0 =  SEL_KDATA;
  TaskStateSegment->ds =  SEL_KDATA;
  TaskStateSegment->es =  SEL_KDATA;
  TaskStateSegment->ss = SEL_KDATA;
  TaskStateSegment->esp = INITTCB_ADDRESS + TCBESP;
  TaskStateSegment->esp0 = INITTCB_ADDRESS + TCBESP;

  // 设置内核文本段（4GB读取和执行，环0）
  seginit(&syspage->gdt[GDT_KTEXT], 0, 0x100000, DESCRIPTOR_CODE | DESCRIPTOR_DPL0 | DESCRIPTOR_READ | DESCRIPTOR_PRESENT, DESCRIPTOR_BIG | DESCRIPTOR_BIG_LIM);

  // 设置内核数据段（4GB读写，环0）
  seginit(&syspage->gdt[GDT_KDATA], 0, 0x100000, DESCRIPTOR_DATA | DESCRIPTOR_DPL0 | DESCRIPTOR_WRITE | DESCRIPTOR_PRESENT, DESCRIPTOR_BIG | DESCRIPTOR_BIG_LIM);

  // 设置用户文本段（2GB读取和执行，响铃3）
  seginit(&syspage->gdt[GDT_UTEXT], 0, BTOP(OSBASE), DESCRIPTOR_CODE | DESCRIPTOR_DPL3 | DESCRIPTOR_READ | DESCRIPTOR_PRESENT, DESCRIPTOR_BIG | DESCRIPTOR_BIG_LIM);

  // 设置用户数据段（2GB读写，环3）
  seginit(&syspage->gdt[GDT_UDATA], 0, BTOP(OSBASE), DESCRIPTOR_DATA | DESCRIPTOR_DPL3 | DESCRIPTOR_WRITE | DESCRIPTOR_PRESENT, DESCRIPTOR_BIG | DESCRIPTOR_BIG_LIM);

  // 设置TSS段
  seginit(&syspage->gdt[GDT_TSS], (unsigned long) &syspage->TaskStateSegment, sizeof(struct TaskStateSegment), DESCRIPTOR_TSS | DESCRIPTOR_DPL0 | DESCRIPTOR_PRESENT, 0);

  // 设置TIB段
  seginit(&syspage->gdt[GDT_TIB], 0, PAGESIZE, DESCRIPTOR_DATA | DESCRIPTOR_DPL3 | DESCRIPTOR_WRITE | DESCRIPTOR_PRESENT, 0);

  // 将GDT设置为新的段描述符
  gdtsel.limit = (sizeof(struct segment) * MAXGDT) - 1;
  gdtsel.dt = syspage->gdt;
  __asm { lgdt gdtsel }

  // 使所有LDT选择器无效
  ldtnull = 0;
  __asm { lldt [ldtnull] };

  // 在syspage中将IDT设置为IDT
  idtsel.limit = (sizeof(struct gate) * MAXIDT) - 1;
  idtsel.dt = syspage->idt;
  __asm { lidt idtsel }

  // 用新的TSS段加载任务寄存器
  tssval = SEL_TSS;
  __asm { ltr tssval };
}

//复制RAM磁盘
void copy_ramdisk(char *bootimg) {
  struct superblock *super = (struct superblock *) (bootimg + SECTORSIZE);
  int i;
  int rdpages;
    
  // 将ram磁盘复制到堆
  if (!bootimg) panic("no boot image");
  if (super->signature != DFS_SIGNATURE) panic("invalid DFS signature on initial RAM disk");
  
  initrd_size = (1 << super->log_block_size) * super->block_count;
  rdpages = PAGES(initrd_size);
  initrd = alloc_heap(rdpages);

  if (super->compress_size != 0) {
    char *zheap;
    unsigned int zofs;
    unsigned int zsize;

    kprintf("Uncompressing boot image\n");

    // 复制映像的未压缩部分
    memcpy(initrd, bootimg, super->compress_offset);

    // 解压缩图像的压缩部分
    zheap = new_heap(64 * 1024 / PAGESIZE);
    zofs = super->compress_offset;
    zsize = super->compress_size;
    unzip(bootimg + zofs, zsize, initrd + zofs, initrd_size - zofs, zheap, 64 * 1024);
  } else {
    memcpy(initrd, bootimg, initrd_size);
  }

  // 将初始ram磁盘映射到syspages
  for (i = 0; i < rdpages; i++) {
    syspagetable[PTEIDX(INITRD_ADDRESS) + i] = ((unsigned long) initrd + i * PAGESIZE) | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;
  }

  kprintf("%d KB boot image found\n", initrd_size / 1024);
}
//获取显卡配置
int get_video_config(char *opts) {
  int param[3];
  int i;

  if (!opts || strncmp(opts, "video=", 6) != 0)
  {
	  return 0;
  }
  opts += 6;
  for (i = 0; i < 3; ++i)
  {
    int num = 0;
    while (*opts >= '0' && *opts <= '9')
    {
      num = num * 10 + (*opts++ - '0');
    }
    if (num == 0)
    {
    	return 0;
    }
    param[i] = num;
    if (*opts == 'x')
    {
    	opts++;
    }
  }
  x_resolution = param[0];
  y_resolution = param[1];
  bpp = param[2];
  return 1;
}

void setup_video_mode()
{
	static struct vesa_info info;
	int rc;
	unsigned short *modes;
	unsigned short mode;

	// 从内核选项获取请求的视频分辨率
	if (!get_video_config(syspage->bootparams.kernel_options))
	{
	  return;
	}
	// 获取VESA信息
	memcpy(info.vesa_signature, "VBE2", 4);
	rc = vesa_get_info(&info);
	if (rc !=  0x004F) return;

	// 查找匹配的VESA模式.
	modes = (unsigned short *) VESA_ADDR(info.video_mode_ptr);
	while ((mode = *modes++) != 0xFFFF)
	{
		struct vesa_mode_info *mi = (struct vesa_mode_info *) scratch;
		rc = vesa_get_mode_info(mode, mi);
		if (rc !=  0x004F) continue;

		// 检查这是否是支持线性帧缓冲区的图形模式
		if ((mi->attributes & 0x90) != 0x90) continue;

		// 检查这是压缩像素模式还是直接彩色模式
		if (mi->memory_model != 4 && mi->memory_model != 6) continue;

		// 检查资源是否匹配
		if (mi->x_resolution == x_resolution && mi->y_resolution == y_resolution && mi->bits_per_pixel == bpp)
		{
		  // 将视频模式信息复制到syspage
		  memcpy(syspage->vgainfo, mi, sizeof(struct vesa_mode_info));

		  // 切换到图形模式
		  kprintf(" (vga mode %d: %dx%dx%d)", mode, mi->x_resolution, mi->y_resolution, mi->bits_per_pixel);
		  vesa_set_mode(mode);
		}
	}
}
/* 操作系统启动过程步骤4：引导加载程序
 * bootparams：引导启动参数
 * reserved:保留的*/
void __stdcall start(void *hmod, struct bootparams *bootparams, int reserved)
{

	char *bootimg = bootparams->bootimg;                 //bootimg = 0x0
	/*1.确定内存大小*/
    setup_memory(&bootparams->memmap);                   // bootparams->memmap = 0x6 。确定内存（RAM）的大小
	kprintf("%d MB RAM\n", mem_end / (1024 * 1024));     //mem_end = 0x7ff0000
    /*2.堆分配从1MB开始*/
    heap = (char *) HEAP_START;
    /*3.设置页表*/
    setup_page_tables();// 设置页表

	bootdrive = bootparams->bootdrv;
	if ((bootdrive & 0xF0) == 0xF0)
	{
	/*10.如果存在初始ram引导磁盘，请将其复制到高内存.*/
	copy_ramdisk(bootimg);
	/*11.从启动磁盘加载内核*/
	load_kernel(bootdrive);
	} else
	{
	init_biosdisk();
	load_kernel(bootdrive);
	}

	/*14.将启动参数复制到syspage*/
	memcpy(&syspage->bootparams, bootparams, sizeof(struct bootparams));
	syspage->ldrparams.heapstart = HEAP_START;
	syspage->ldrparams.heapend = (unsigned long) heap;
	syspage->ldrparams.memend = mem_end;
	syspage->ldrparams.bootdrv = bootdrive;
	syspage->ldrparams.bootpart = bootpart;
	syspage->ldrparams.initrd_size = initrd_size;
	memcpy(syspage->biosdata, (void *) 0x0400, 256);
	// 设置视频模式。
	setup_video_mode();

	// 内核选项位于引导参数块中
	kernel_options = syspage->bootparams.kernel_options;

	/*12.设置页面目录（CR3）并启用分页（CR0中的PG位）*/
	__asm {
	mov eax, dword ptr [pdir]
	mov cr3, eax
	mov eax, cr0
	or eax, 0x80000000
	mov cr0, eax
    }

    /*13.在syspage中设置新描述符（GDT、LDT、IDT和TSS）*/
    setup_descriptors();

    /*15.重新加载段寄存器*/
    __asm {
    mov ax, SEL_KDATA
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    push SEL_KTEXT
    push offset startpg
    retf
    startpg:
    }

    /*16.切换到初始内核堆栈并跳转到内核*/
    __asm {
    mov esp, INITTCB_ADDRESS + TCBESP
    push 0
    push dword ptr [kernel_options]
    push OSBASE
    call dword ptr [kernel_entry]
    cli
    hlt
    }
}
