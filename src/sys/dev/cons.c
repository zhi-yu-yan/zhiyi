
// 控制台设备驱动程序

#include <os/krnl.h>

#define CTRL(c) ((c) - 'A' + 1)

#define SERIAL_CONSOLE_PORT 0x3F8 // COM1  串行控制台端口

static dev_t consdev = NODEV;
static int cursoff = 0;
static unsigned int kbd_timeout = INFINITE;
int serial_console = 0;

void sound(unsigned short freq)
{
  unsigned short freqdiv;
  unsigned char b;

  freqdiv = 1193180 / freq;

  // 计数器2选择，二进制16位计数器，第一位0-7
  outp(0x43, 0xB6); 
         
  // First bits 0-7
  outp(0x42, (unsigned char) freqdiv);
  
  // Then bits 8-15
  outp(0x42, (unsigned char) (freqdiv >> 8)); 

  // 仅当位未正确设置时输出
  b = inp(0x61);
  if (b != (b | 3)) outp(0x61, b | 3);
}

void nosound()  {
  unsigned char b;

  // KB controller port B
  b = inp(0x61);

  // Disable speaker + clock 2 gate
  b &= 0xFC;

  // Output
  outp(0x61, b);
}
 
void beep()  {
  sound(1000);
  msleep(250);
  nosound();
}

//初始化串行控制台
void init_serial_console() {
  // 关闭中断
  outp(SERIAL_CONSOLE_PORT + 1, 0);
  
  // 设置115200波特，8位，无奇偶校验，一个停止位
  outp(SERIAL_CONSOLE_PORT + 3, 0x80);
  outp(SERIAL_CONSOLE_PORT + 0, 0x01); // 0x0C = 9600, 0x01 = 115200
  outp(SERIAL_CONSOLE_PORT + 1, 0x00);
  outp(SERIAL_CONSOLE_PORT + 3, 0x03);
  outp(SERIAL_CONSOLE_PORT + 2, 0xC7);
  outp(SERIAL_CONSOLE_PORT + 4, 0x0B);
}

static void serial_console_write(void *buffer, int count) {
  unsigned char *p = buffer;

  while (count-- > 0) {
    while ((inp(SERIAL_CONSOLE_PORT + 5) & 0x20) == 0);
    outp(SERIAL_CONSOLE_PORT, *p++);
  }
}

static int console_ioctl(struct dev *dev, int cmd, void *args, size_t size) {
  int freq;

  switch (cmd) {
    case IOCTL_GETDEVSIZE:
      return 0;

    case IOCTL_GETBLKSIZE:
      return 1;

    case IOCTL_SET_KEYTIMEOUT:
      if (!args || size != 4) return -EINVAL;
      kbd_timeout = *(unsigned int *) args;
      return 0;

    case IOCTL_CTRLALTDEL_ENABLED:
      if (!args || size != 4) return -EINVAL;
      ctrl_alt_del_enabled = *(int *) args;
      return 0;

    case IOCTL_SET_KEYMAP:
      if (!args || size != 4) return -EINVAL;
      return change_keyboard_map_id(*(int *) args);

    case IOCTL_BEEP:
      beep();
      return 0;

    case IOCTL_SOUND:
      if (!args || size != 4) return -EINVAL;
      freq = *(int *) args;
      if (freq < 0 || freq > 0xFFFF) return -EINVAL;

      if (freq == 0) {
        nosound();
      } else {
        sound((unsigned short) freq);
      }
      return 0;

    case IOCTL_REBOOT:
      stop(EXITOS_REBOOT);
      return 0;

    case IOCTL_KBHIT:
      if (cursoff) {
        cursoff = 0;
        show_cursor();
      }
      return kbhit();
  }
  
  return -ENOSYS;
}

static int console_read(struct dev *dev, void *buffer, size_t count, blkno_t blkno, int flags) {
  char *p = (char *) buffer;
  int ch;
  int n;

  for (n = 0; n < (int) count; n++) {
    if (cursoff) {
      cursoff = 0;
      show_cursor();
    }

    ch = getch(n || (flags & DEVFLAG_NBIO) ? 0 : kbd_timeout);
    if (ch < 0) {
      if (n) return n;
      if (ch == -ETIMEOUT && (flags & DEVFLAG_NBIO)) return -EAGAIN;
      return ch;
    }
    
    if (ch < ' ') {
      struct thread *t = self();
      if (ch == CTRL('C') && (t->blocked_signals & (1 << SIGINT)) == 0) send_user_signal(t, SIGINT);
      if (ch == CTRL('Z') && (t->blocked_signals & (1 << SIGTSTP)) == 0) send_user_signal(t, SIGTSTP);
      if (ch == CTRL('\\') && (t->blocked_signals & (1 << SIGABRT)) == 0) send_user_signal(t, SIGABRT);
    }
    
    *p++ = ch;
  }

  return count; 
}

static int console_write(struct dev *dev, void *buffer, size_t count, blkno_t blkno, int flags) {
  if (!cursoff)
  {
    cursoff = 1;
    hide_cursor();
  }

  print_buffer((char *) buffer, count);

  return count;
}

struct driver console_driver = {
  "console",
  DEV_TYPE_STREAM,
  console_ioctl,
  console_read,
  console_write
};

int __declspec(dllexport) console(struct unit *unit, char *opts) {
  dev_t devno = dev_make("console", &console_driver, NULL, NULL);
  init_keyboard(devno, get_num_option(opts, "resetkbd", 0));

  if (serial_console) {
    init_serial();
    consdev = dev_open("com1");
  } else {
    consdev = dev_open("console");
  }

  register_proc_inode("screen", screen_proc, NULL);

  return 0;
}
/*
 * buffer:字符串
 * size:字符串的长度
 * */
void console_print(char *buffer, int size) {
  if (consdev != NODEV) {
    dev_write(consdev, buffer, size, 0, 0);
  } else if (serial_console) {
    serial_console_write(buffer, size);
  } else {
    print_buffer(buffer, size);
  }
}
//初始化控制台
void init_console(int serial)
{


  if (serial_console)
  {
	  init_serial_console();//串行控制台
  }
}
