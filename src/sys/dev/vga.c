
// VGA图形驱动程序。Frame Buffer的核心代码，向上层提供了vga_ioctl(),vga_read(),vga_write()接口，向下
//给硬件设备提供了统一的接口，把接口抽象为一个fb_info结构体，实现一个显示驱动就需要填充这个结构体，并使用 提供的注册 与注销 函数加载和
//卸载驱动。


#include <os/krnl.h>
#include <os/vga.h>

struct vga {
  struct vesa_mode_info *mode;  // vesa图形模式
  unsigned char *fb;            // 帧缓冲区
  unsigned int fbsize;          // 帧缓冲区大小
};

static int vga_ioctl(struct dev *dev, int cmd, void *args, size_t size) {
  struct vga *vga = (struct vga *) dev->privdata;

  switch (cmd) {
    case IOCTL_GETDEVSIZE: //获取设备大小
      return vga->fbsize;

    case IOCTL_GETBLKSIZE:
      return 1;

    case IOCTL_VGA_GET_VIDEO_MODE: //获取视频模式
      if (!args) return -EINVAL;
      if (size > sizeof(struct vesa_mode_info)) size = sizeof(struct vesa_mode_info);
      memcpy(args, vga->mode, size);
      return 0;

    case IOCTL_VGA_GET_FRAME_BUFFER: //获取frame buffer
      if (!args || size != sizeof(unsigned char *))
      {
    	  return -EINVAL;
      }
      *((unsigned char **) args) = vga->fb;
      return 0;
  }

  return -ENOSYS;
}

static int vga_read(struct dev *dev, void *buffer, size_t count, blkno_t blkno, int flags) {
  struct vga *vga = (struct vga *) dev->privdata;

  if (blkno > vga->fbsize) return -EFAULT;
  if (blkno + count > vga->fbsize) count = vga->fbsize - blkno;
  if (count == 0) return 0;
  memcpy(buffer, vga->fb + blkno, count);

  return count;
}

static int vga_write(struct dev *dev, void *buffer, size_t count, blkno_t blkno, int flags) {
  struct vga *vga = (struct vga *) dev->privdata;

  if (blkno > vga->fbsize) return -EFAULT;
  if (blkno + count > vga->fbsize) count = vga->fbsize - blkno;
  if (count == 0) return 0;
  memcpy(vga->fb + blkno, buffer, count);

  return count;
}

static int color_mask(int size, int pos) {
  return size ? ((1 << size) - 1) << pos : 0;
}

static int vgainfo_proc(struct proc_file *pf, void *arg) {
  struct vga *vga = (struct vga *) arg;
  struct vesa_mode_info *info = vga->mode;

  pprintf(pf, "Resolution.......... : %dx%dx%d\n", info->x_resolution, info->y_resolution, info->bits_per_pixel);
  pprintf(pf, "Color resolution.... : R=%d G=%d B=%d A=%d\n", 
          1 << info->red_mask_size, 
          1 << info->green_mask_size, 
          1 << info->blue_mask_size, 
          1 << info->reserved_mask_size);
  pprintf(pf, "Character size...... : %dx%d\n", info->x_char_size, info->y_char_size);
  pprintf(pf, "VGA attributes...... : %x\n", info->attributes);
  pprintf(pf, "Bytes per scanline.. : %d\n", info->bytes_per_scanline);
  pprintf(pf, "Memory model........ : %d\n", info->memory_model);
  pprintf(pf, "Planes.............. : %d\n", info->number_of_planes);
  pprintf(pf, "Banks............... : %d\n", info->number_of_banks);
  pprintf(pf, "Bank size........... : %d\n", info->bank_size);
  pprintf(pf, "Image pages......... : %d\n", info->number_of_image_pages);
  pprintf(pf, "Color masks......... : R=%08x G=%08x B=%08x A=%08x\n", 
          color_mask(info->red_mask_size, info->red_mask_pos),
          color_mask(info->green_mask_size, info->green_mask_pos),
          color_mask(info->blue_mask_size, info->blue_mask_pos),
          color_mask(info->reserved_mask_size, info->reserved_mask_pos));
  pprintf(pf, "Reserved page....... : %d\n", info->reserved_page);
  pprintf(pf, "Direct color mode... : %d\n", info->direct_color_mode_info);
  pprintf(pf, "Frame buffer addr... : phys: %08x virt: %08X size: %d\n", info->phys_base_ptr, vga->fb, vga->fbsize);
  pprintf(pf, "Off-screen memory... : %08x (%d bytes)\n", info->off_screen_mem_offset, info->off_screen_mem_size);
  pprintf(pf, "Window A segment.... : %d\n", info->wina_segment);
  pprintf(pf, "Window B segment.... : %d\n", info->winb_segment);
  pprintf(pf, "Window func......... : %08x\n", info->win_func_ptr);

  return 0;
}

struct driver vga_driver = {
  "vga",
  DEV_TYPE_BLOCK,
  vga_ioctl,
  vga_read,
  vga_write
};

int __declspec(dllexport) vga(struct unit *unit) {
  struct vga *vga;
  struct vesa_mode_info *mode = (struct vesa_mode_info *) syspage->vgainfo;
  dev_t devno;

  // 检查图形模式是否已启用
  if (!mode->phys_base_ptr) return 0;

  // 为设备分配内存
  vga = kmalloc(sizeof(struct vga));
  if (vga == NULL) return -ENOMEM;
  memset(vga, 0, sizeof(struct vga));
  vga->mode = mode;

  // 使帧缓冲区用户可访问
  vga->fbsize = mode->y_resolution * mode->bytes_per_scanline;
  vga->fb = miomap(mode->phys_base_ptr, vga->fbsize, PAGE_READWRITE);
  if (!vga->fb) return -ENOMEM;

  devno = dev_make("fb#", &vga_driver, unit, vga);
  kprintf(KERN_INFO "%s: VGA %dx%dx%d, frame buffer %x (%d)\n", 
          device(devno)->name, 
          mode->x_resolution, mode->y_resolution, mode->bits_per_pixel,
          mode->phys_base_ptr, vga->fbsize);

  // VGA 信息
  register_proc_inode("vgainfo", vgainfo_proc, vga);

  return 0;
}

