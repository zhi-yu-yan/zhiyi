/****************************************************************************************************
 * 描述：内核初始化.内核初始化子系统并启动主任务.主内核任务加载设备驱动程序、装载文件系统并将os.dll加载到用户空间.
 * www.880.xin
 ****************************************************************************************************/

#include <os/krnl.h>

#ifdef BSD
char *copyright = 
"版权(C) 2023\n"
;
#endif

#ifdef GPL
char *copyright = 
"版权(C) 2023\n"
;
#endif

#define ONPANIC_HALT      EXITOS_HALT
#define ONPANIC_REBOOT    EXITOS_REBOOT
#define ONPANIC_DEBUG     EXITOS_DEBUG
#define ONPANIC_POWEROFF  EXITOS_POWEROFF

struct thread *mainthread;
struct section *krnlcfg;
int onpanic = ONPANIC_HALT;
struct peb *peb;
char kernel_options[KERNEL_OPTONS_MAX_LENGTH];

void main(void *arg);

int license() {
  return LICENSE;
}

void stop(int mode) {
  suspend_all_user_threads();
  umount_all();
  tcp_shutdown();
  msleep(200);

  switch (mode) {
    case EXITOS_HALT:
      kprintf("内核: 系统已停止\n");
      break;

    case EXITOS_POWEROFF:
      kprintf("内核: 断电...\n");
      poweroff();
      break;

    case EXITOS_REBOOT:
      kprintf("内核: 重新启动...\n");
      reboot();
      break;

    case EXITOS_DEBUG:
      dbg_break();
      break;
  }

  while (1) {
    cli();
    halt();
  }
}

void panic(char *msg) {
  static int inpanic = 0;

  if (inpanic)
  {
    kprintf(KERN_EMERG "double panic: %s, halting\n", msg);
    cli();
    halt();
  }

  inpanic = 1;
  kprintf(KERN_EMERG "panic: %s\n", msg);

  if (onpanic == ONPANIC_DEBUG) {
    if (debugging) dbg_output(msg);
    dbg_break();
  } else {
    stop(onpanic);
  }
}

static int load_kernel_config() {
  struct file *f;
  int size;
  int rc;
  struct stat64 buffer;
  char config[MAXPATH];
  char *props;

  get_option(kernel_options, "config", config, sizeof(config), "/boot/krnl.ini");

  rc = open(config, O_RDONLY | O_BINARY, 0, &f);
  if (rc < 0) return rc;

  fstat(f, &buffer);
  size = (int) buffer.st_size;

  props = (char *) kmalloc(size + 1);
  if (!props)  {
    close(f);
    destroy(f);
    return -ENOMEM;
  }

  rc = read(f, props, size);
  if (rc < 0) {
    free(props);
    close(f);
    destroy(f);
    return rc;
  }

  close(f);
  destroy(f);

  props[size] = 0;

  krnlcfg = parse_properties(props);
  free(props);

  return 0;
}

static void init_filesystem() {
  int rc;
  char bootdev[8];
  char rootdev[128];
  char rootfs[32];
  char *rootfsopts;
  char fsoptbuf[128];

  // 初始化内置文件系统
  init_vfs();
  init_dfs();
  init_devfs();
  init_procfs();
  init_pipefs();
  init_smbfs();
  init_cdfs();
 
  // 确定引导设备
  if ((syspage->ldrparams.bootdrv & 0xF0) == 0xF0) {
    create_initrd();
    strcpy(bootdev, "initrd");
  } else if (syspage->ldrparams.bootdrv & 0x80) {
    if (syspage->ldrparams.bootpart == -1) {
      sprintf(bootdev, "hd%c", '0' + (syspage->ldrparams.bootdrv & 0x7F));
    } else {
      sprintf(bootdev, "hd%c%c", '0' + (syspage->ldrparams.bootdrv & 0x7F), 'a' + syspage->ldrparams.bootpart);
    }
  }
  else {
    sprintf(bootdev, "fd%c", '0' + (syspage->ldrparams.bootdrv & 0x7F));
  }

  // 如果找不到默认启动设备，请尝试虚拟设备
  if (devno(bootdev) == NODEV && devno("vd0") != NODEV) strcpy(bootdev, "vd0");

  // 确定根文件系统
  get_option(kernel_options, "rootdev", rootdev, sizeof(rootdev), bootdev);
  get_option(kernel_options, "rootfs", rootfs, sizeof(rootfs), "dfs");
  rootfsopts = get_option(kernel_options, "rootopts", fsoptbuf, sizeof(fsoptbuf), NULL);

  kprintf(KERN_INFO "mount: root on %s\n", rootdev);

  // 装载文件系统
  rc = mount(rootfs, "/", rootdev, rootfsopts, NULL);
  if (rc < 0) panic("装载根文件系统时出错");

  rc = mount("devfs", "/dev", NULL, NULL, NULL);
  if (rc < 0) panic("安装dev文件系统时出错");

  rc = mount("procfs", "/proc", NULL, NULL, NULL);
  if (rc < 0) panic("装载proc文件系统时出错");
}

static int version_proc(struct proc_file *pf, void *arg) {
  hmodule_t krnl = (hmodule_t) OSBASE;
  struct verinfo *ver;
  time_t ostimestamp;
  char osname[32];
  struct tm tm;

  ostimestamp = get_image_header(krnl)->header.timestamp;
  gmtime_r(&ostimestamp, &tm);

  ver = get_version_info(krnl);
  if (ver)  {
    if (get_version_value(krnl, "ProductName", osname, sizeof(osname)) < 0) strcpy(osname, "Zhiyi");
    pprintf(pf, "%s version %d.%d.%d.%d", osname, ver->file_major_version, ver->file_minor_version, ver->file_release_number, ver->file_build_number);

    if (ver->file_flags & VER_FLAG_PRERELEASE) pprintf(pf, " prerelease");
    if (ver->file_flags & VER_FLAG_PATCHED) pprintf(pf, " patch");
    if (ver->file_flags & VER_FLAG_PRIVATEBUILD) pprintf(pf, " private");
    if (ver->file_flags & VER_FLAG_DEBUG) pprintf(pf, " debug");
  } else {
    pprintf(pf, "%s version %d.%d.%d.%d", OS_NAME, OS_MAJ_VERS, OS_MIN_VERS, OS_RELEASE, OS_BUILD);
  }

  pprintf(pf, " (Built %04d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
#ifdef _MSC_VER
  pprintf(pf, " with MSVC %d.%02d on WIN32", _MSC_VER / 100, _MSC_VER % 100);
#endif
#ifdef _TCC_VER
  pprintf(pf, " with ZHI %s on %s", _TCC_VER, _TCC_PLATFORM);
#endif
  pprintf(pf, ")\n");

  return 0;
}

static int copyright_proc(struct proc_file *pf, void *arg) {
  hmodule_t krnl = (hmodule_t) OSBASE;
  char copy[128];
  char legal[128];

  if (get_version_value(krnl, "LegalCopyright", copy, sizeof(copy)) < 0) strcpy(copy, OS_COPYRIGHT);
  if (get_version_value(krnl, "LegalTrademarks", legal, sizeof(legal)) < 0) strcpy(legal, OS_LEGAL);

  version_proc(pf, arg);
  pprintf(pf, "%s %s\n\n", copy, legal);
  proc_write(pf, copyright, strlen(copyright));
  return 0;
}
/* 操作系统启动过程步骤5：内核启动
 * */
void __stdcall start(void *hmod, char *opts, int reserved2)
{
  strcpy(kernel_options, opts);// 复制内核选项
  if (get_option(opts, "silent", NULL, 0, NULL) != NULL)
  {
	  kprint_enabled = 0;
  }
  if (get_option(opts, "serialconsole", NULL, 0, NULL) != NULL)
  {
	  serial_console = 1;/*串行控制台*/
  }
  init_video();//初始化屏幕
  init_console();// 初始化控制台.

  kprintf(KERN_INFO "5:neihe  \n");// 显示横幅

  if (*kernel_options)
  {
	  kprintf(KERN_INFO "options: %s\n", kernel_options);
  }
  init_mach();// 初始化计算机
  init_cpu();// 初始化CPU
  /*1.初始化内存管理子系统*/
  init_pfdb();  /*1.1初始化页面框架数据库*/
  init_pdir();  /*1.2初始化页面目录*/
  init_kmem();  /*1.3初始化内核堆*/
  init_malloc();/*1.4初始化内核分配器*/
  init_vmm();   /*1.5初始化虚拟内存管理器*/
  flushtlb();// Flush tlb
  // 注册内存管理进程
  register_proc_inode("memmap", memmap_proc, NULL);
  register_proc_inode("memusage", memusage_proc, NULL);
  register_proc_inode("memstat", memstat_proc, NULL);
  register_proc_inode("physmem", physmem_proc, NULL);
  register_proc_inode("pdir", pdir_proc, NULL);
  register_proc_inode("virtmem", virtmem_proc, NULL);
  register_proc_inode("kmem", kmem_proc, NULL);
  register_proc_inode("kmodmem", kmodmem_proc, NULL);
  register_proc_inode("kheap", kheapstat_proc, NULL);
  register_proc_inode("vmem", vmem_proc, NULL);
  register_proc_inode("cpu", cpu_proc, NULL);
  /*2.初始化线程控制子系统*/
  init_pic();/*2.1初始化中断、浮点支持和实时时钟*/
  init_trap();/*2.2初始化调度程序*/
  init_fpu();
  init_pit();
  init_timers();// 初始化计时器、调度器和句柄管理器
  init_sched();
  init_handles();
  init_syscall();
  sti();        /*2.3启用中断并校准延迟*/
  calibrate_delay();
  /*3.启动主任务并分派到空闲任务*/
  mainthread = create_kernel_thread(main, 0, PRIORITY_NORMAL, "init");
  /*4.处理闲置任务*/
  idle_task();
}

void init_net() {
  stats_init();
  netif_init();
  ether_init();
  pbuf_init();
  arp_init();
  ip_init();
  udp_init();
  raw_init();
  dhcp_init();
  tcp_init();
  socket_init();
  loopif_init();

  register_ether_netifs();
}
/* 操作系统启动过程步骤6:主内核任务
 * */
void main(void *arg)
{
  kprintf(", begin of main function \n");
  unsigned long *stacktop;
  struct thread *t = self();

  void *imgbase;
  void *entrypoint;
  unsigned long stack_reserve;
  unsigned long stack_commit;
  struct image_header *imghdr;
  struct verinfo *ver;
  int rc;
  char *str;
  struct file *cons;
  char *console;

  /*11.分配并初始化PEB(进程环境块)，vmm.c里的vmalloc()函数*/
  peb = vmalloc((void *) PEB_ADDRESS, PAGESIZE, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE, 'PEB', NULL);
  if (!peb)
  {
	  panic("无法分配PEB");
  }
  memset(peb, 0, PAGESIZE);
  peb->fast_syscalls_supported = (cpu.features & CPU_FEATURE_SEP) != 0;

  /*1.枚举根主机总线和单元*/
  enum_host_bus();

  /*2.初始化引导设备（软盘和硬盘）*/
  init_hd();
  init_fd();
  init_vblk();

  /*3.初始化内置文件系统（dfs、devfs、procfs和pipefs）*/
  init_filesystem();

  /*5.加载内核配置（/etc/krnl.ini）*/
  load_kernel_config();

  // 确定内核紧急操作
  str = get_property(krnlcfg, "kernel", "onpanic", "halt");
  if (strcmp(str, "halt") == 0) {
    onpanic = ONPANIC_HALT;
  } else if (strcmp(str, "reboot") == 0 || strcmp(str, "重启") == 0) {
    onpanic = ONPANIC_REBOOT;
  } else if (strcmp(str, "debug") == 0) {
    onpanic = ONPANIC_DEBUG;
  } else if (strcmp(str, "poweroff") == 0) {
    onpanic = ONPANIC_POWEROFF;
  }

  // 设置路径分隔符
  pathsep = *get_property(krnlcfg, "kernel", "pathsep", "");
  if (pathsep != PS1 && pathsep != PS2) pathsep = PS1;
  t->curdir[0] = pathsep;
  t->curdir[1] = 0;
  peb->pathsep = pathsep;

  /*6.初始化内核模块加载器*/
  init_kernel_modules();

  // 从内核版本资源获取os版本信息
  get_version_value((hmodule_t) OSBASE, "ProductName", peb->osname, sizeof peb->osname);
  peb->ostimestamp = get_image_header((hmodule_t) OSBASE)->header.timestamp;
  ver = get_version_info((hmodule_t) OSBASE);
  if (ver) {
    memcpy(&peb->osversion, ver, sizeof(struct verinfo));
  } else {
    strcpy(peb->osname, OS_NAME);
    peb->osversion.file_major_version = OS_MAJ_VERS;
    peb->osversion.file_minor_version = OS_MIN_VERS;
    peb->osversion.file_release_number = OS_RELEASE;
    peb->osversion.file_build_number = OS_BUILD;
  }

  /*8.绑定、加载和初始化设备驱动程序*/
  install_drivers();

  /*9.初始化网络*/
  init_net();

  // 安装/proc/version和/proc/copyright处理程序
  register_proc_inode("version", version_proc, NULL);
  register_proc_inode("copyright", copyright_proc, NULL);

  /*10.为stdin、stdout和stderr分配句柄*/
  console = get_property(krnlcfg, "kernel", "console", serial_console ? "/dev/com1" : "/dev/console");
  rc = open(console, O_RDWR, S_IREAD | S_IWRITE, &cons);
  if (rc < 0) panic("no console");
  cons->flags |= F_TTY;
  if (halloc(&cons->iob.object) != 0) panic("unexpected stdin handle");
  if (halloc(&cons->iob.object) != 1) panic("unexpected stdout handle");
  if (halloc(&cons->iob.object) != 2) panic("unexpected stderr handle");

  /*12.将/bin/os.dll加载到用户空间*/
  imgbase = load_image_file(get_property(krnlcfg, "kernel", "osapi", "/boot/os.dll"), 1);
  if (!imgbase) panic("无法加载os.dll");
  imghdr = get_image_header(imgbase);
  stack_reserve = imghdr->optional.size_of_stack_reserve;
  stack_commit = imghdr->optional.size_of_stack_commit;
  entrypoint = get_entrypoint(imgbase);

  /* 13.初始化初始用户线程（堆栈和tib）*/
  if (init_user_thread(t, entrypoint) < 0) panic("无法初始化初始用户线程");
  if (allocate_user_stack(t, stack_reserve, stack_commit) < 0) panic("无法为初始用户线程分配堆栈");
  t->hndl = halloc(&t->object);
  hprotect(t->hndl);
  mark_thread_running();

  kprintf(KERN_INFO "neicun: %dMB(zongshu), %dKB(yiyong), %dKB(kongjian), %dKB(baoliu)\n",
          maxmem * PAGESIZE / (1024 * 1024), 
          (totalmem - freemem) * PAGESIZE / 1024, 
          freemem * PAGESIZE / 1024, (maxmem - totalmem) * PAGESIZE / 1024);


  //在堆栈上放置启动例程的参数
  stacktop = (unsigned long *) t->tib->stacktop;
  *(--stacktop) = 0;
  *(--stacktop) = 0;
  *(--stacktop) = (unsigned long) imgbase;
  *(--stacktop) = 0;

  /* 14.os.dll中的调用入口点（跳转到用户模式）
   * */
  __asm {
    mov eax, stacktop
    mov ebx, entrypoint

    push SEL_UDATA + SEL_RPL3
    push eax
    pushfd
    push SEL_UTEXT + SEL_RPL3
    push ebx
    mov ax, SEL_UDATA + SEL_RPL3
    mov ds, ax
    mov es, ax
    IRETD
  }

}
