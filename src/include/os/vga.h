//
// vga.h
//
// VESA BIOS扩展定义

#ifndef VGA_H
#define VGA_H

#define VESA_ADDR(ptr) (((ptr) & 0xFFFF) | ((ptr) >> 12))

#pragma pack(push, 1)

// VGA驱动程序ioctl代码

#define IOCTL_VGA_GET_VIDEO_MODE   1024
#define IOCTL_VGA_GET_FRAME_BUFFER 1025

// VBE(VESA BIOS Extension)VESA基本输入输出扩展信息

typedef struct vesa_info {
  unsigned char vesa_signature[4];   //vesa签名
  unsigned short vesa_version;       //vesa版本
  unsigned long oem_string_ptr;      //指向oem字符串的指针，该指针是一个16位的selector:offset形式的指针，在实模式下可以直接使用。
  unsigned char capabilities[4];     //容量
  unsigned long video_mode_ptr;      //是指向视频模式列表的指针，与oem_string_ptr类型一样
  unsigned short total_memory;       //是64kb内存块的个数
  unsigned short oem_software_rev;
  unsigned long  oem_vendor_name_ptr;//是指向厂商名字符串的指针
  unsigned long  oem_product_name_ptr;//是指向产品名字符串的指针
  unsigned long  oem_product_rev_ptr;
  unsigned char  reserved[222];
  unsigned char  oem_data[256];
}VbeInfoBlock;

// VESA模式信息

typedef struct vesa_mode_info {
  unsigned short attributes;//这个字段描述了图形模式的一些重要属性。其中最重要的是第4位和第7位。第4位为1表示图形模式(Graphics mode)，为0表示文本模式(Text mode)。第7位为1表示线性帧缓冲模式(Linear frame buffer mode)，为0表示非线性帧缓冲模式。我们主要要检查这两个位。
  unsigned char wina_attributes;  //窗口A属性，第0位表示是否存在
  unsigned char winb_attributes;  //窗口B属性
  unsigned short win_granularity; //窗口点数
  unsigned short win_size;        //窗口打大小
  unsigned short wina_segment;    //窗口A段地址
  unsigned short winb_segment;    //窗口B段地址
  unsigned long win_func_ptr;     //指向窗口置位功能的指针
  unsigned short bytes_per_scanline;//每行字节数

  unsigned short x_resolution;//x水平分辨率
  unsigned short y_resolution;//y垂直分辨率
  unsigned char x_char_size;  //字符宽度
  unsigned char y_char_size;  //字符高度
  unsigned char number_of_planes;//存储器位面数
  unsigned char bits_per_pixel;/*表示该视频模式每个像素所占的位数*/
  unsigned char number_of_banks;
  unsigned char memory_model;
  unsigned char bank_size;
  unsigned char number_of_image_pages;
  unsigned char reserved_page;

  unsigned char red_mask_size;
  unsigned char red_mask_pos;
  unsigned char green_mask_size;
  unsigned char green_mask_pos;
  unsigned char blue_mask_size;
  unsigned char blue_mask_pos;
  unsigned char reserved_mask_size;
  unsigned char reserved_mask_pos;
  unsigned char direct_color_mode_info;

  unsigned long phys_base_ptr;//这是一个非常重要的字段，它给出了平坦内存帧缓冲区的物理地址，你可以理解为显存的首地址。如果每个像素占32位的话，屏幕左上角第一个点所占的缓冲区就是phys_base_ptr所指的第一个4个字节。按照先行后列的顺序，每个像素点所占缓冲区依次紧密排列。我们要想在屏幕上画出像素点，就得操作以phys_base_ptr为起始的物理内存空间。
  unsigned long off_screen_mem_offset;
  unsigned short off_screen_mem_size;

  unsigned char reserved[206];
}VbeModeInfoBlock;

#pragma pack(pop)

#endif

