/****************************************************************************************************
 * 描述：系统分页定义
 * www.880.xin
 ****************************************************************************************************/


#ifndef SYSPAGE_H
#define SYSPAGE_H

#define GDT_TO_SEL(gdt) ((gdt) << 3)

#define GDT_NULL     0
#define GDT_KTEXT    1
#define GDT_KDATA    2
#define GDT_UTEXT    3
#define GDT_UDATA    4
#define GDT_TSS      5
#define GDT_TIB      6
#define GDT_AUX1     7
#define GDT_APM40    8
#define GDT_APMCS    9
#define GDT_APMCS16  10
#define GDT_APMDS    11
#define GDT_PNPTEXT  12
#define GDT_PNPDATA  13
#define GDT_PNPTHUNK 14
#define GDT_AUX2     15

#define MAXGDT 16
#define MAXIDT 64

#define SEL_NULL     GDT_TO_SEL(GDT_NULL)
#define SEL_KTEXT    GDT_TO_SEL(GDT_KTEXT)
#define SEL_KDATA    GDT_TO_SEL(GDT_KDATA)
#define SEL_UTEXT    GDT_TO_SEL(GDT_UTEXT)
#define SEL_UDATA    GDT_TO_SEL(GDT_UDATA)
#define SEL_TSS      GDT_TO_SEL(GDT_TSS)
#define SEL_TIB      GDT_TO_SEL(GDT_TIB)
#define SEL_AUX1     GDT_TO_SEL(GDT_AUX1)
#define SEL_APM40    GDT_TO_SEL(GDT_APM40)
#define SEL_APMCS    GDT_TO_SEL(GDT_APMCS)
#define SEL_APMCS16  GDT_TO_SEL(GDT_APMCS16)
#define SEL_APMDS    GDT_TO_SEL(GDT_APMDS)
#define SEL_PNPTEXT  GDT_TO_SEL(GDT_PNPTEXT)
#define SEL_PNPDATA  GDT_TO_SEL(GDT_PNPDATA)
#define SEL_PNPTHUNK GDT_TO_SEL(GDT_PNPTHUNK)
#define SEL_AUX2     GDT_TO_SEL(GDT_AUX2)

#define SEL_RPL0    0
#define SEL_RPL3    3

#define KERNEL_OPTONS_ADDRESS_OFFSET      0x1A   // 操作系统加载程序中内核选项地址的偏移量
#define KERNEL_OPTONS_MAX_LENGTH         128    // 内核选项的最大长度

#define VESA_MODE_INFORMATION_BUFFER_SIZE        512    // VESA模式信息缓冲区的大小

// APM安装检查标志

#define APM_16_BIT_SUPPORT      0x0001
#define APM_32_BIT_SUPPORT      0x0002
#define APM_IDLE_SLOWS_CLOCK    0x0004
#define APM_BIOS_DISABLED       0x0008
#define APM_BIOS_DISENGAGED     0x0010

// APM BIOS参数块

struct apmparams {
  unsigned short version;      // APM版本（BCD格式）
  unsigned short flags;        // 来自安装检查的APM标志
  unsigned short cseg32;       // APM 32位代码段（实模式段基地址）
  unsigned short entry;        // APM BIOS入口点的偏移
  unsigned short cseg16;       // APM 16位代码段（实模式段基地址）
  unsigned short dseg;         // APM数据段（实模式段基地址）
  unsigned short cseg32len;    // APM BIOS 32位代码段长度
  unsigned short cseg16len;    // APM BIOS 16位代码段长度
  unsigned short dseglen;      // APM BIOS数据段长度
};

// 内存映射

#define MAX_MEMERY_ENTRIES    32 //(最大内存条目)

#define MEMORY_TYPE_RAM       1  //内存
#define MEMORY_TYPE_RESERVED  2  //保留的
#define MEMORY_TYPE_ACPI      3  //高级配置与电源接口(Advanced Configuration and Power Interface)
#define MEMORY_TYPE_NVS       4  //NVS是一种非易失性存储器.NVS一次最多可以存储256字节的数据。

#pragma pack(push, 1)

//0xF0000-0xFFFFF (ROM-BIOS)的物理地址
struct memmap {
  int count;              // 内存映射中的条目数
  struct mementry {
    unsigned __int64 addr;     // 内存段开始
    unsigned __int64 size;     // 内存段大小
    unsigned long type;        // 内存段类型
  } entry[MAX_MEMERY_ENTRIES];     //最大32
};

// 引导参数块

struct bootparams {
  int bootdrv;                 // 启动驱动器:
                               //  0x00第一张软盘
                               //  0x01第二张软盘
                               //  0x80第一个硬盘
                               //  0x81第二个硬盘
                               //  0xFD CD-ROM（1.44 MB软盘仿真）
                               //  0xFE PXE网络引导
                               //  0xFF CD-ROM（无仿真）
  char *bootimg;               // 启动映像的物理地址
  char kernel_options[KERNEL_OPTONS_MAX_LENGTH]; // 内核选项（由mkdfs设置）
  struct apmparams apm;        // APM BIOS参数
  struct memmap memmap;        // 来自BIOS的系统内存映射0xF0000-0xFFFFF
};

#pragma pack(pop)

// 加载参数块

struct ldrparams {
  unsigned long heapstart;      // 启动堆的开始
  unsigned long heapend;        // 启动堆结束
  unsigned long memend;         // RAM结束
  int bootdrv;                  // 启动驱动器
  int bootpart;                 // 启动分区
  unsigned long initrd_size;    // 初始RAM磁盘大小
};

// 系统分页

struct syspage {
  struct TaskStateSegment TaskStateSegment;                       // 任务状态分段
  struct segment gdt[MAXGDT];           // 全局描述符表(所有进程的总目录表)
  struct gate idt[MAXIDT];              // 中断描述符表（保护模式下所有中断服务程序的入口，类似于实模式下的中断向量表）
  struct bootparams bootparams;         // 引导参数块
  struct ldrparams ldrparams;           // 装载参数块
  unsigned char biosdata[256];          // BIOS数据区域的副本
  unsigned char vgainfo[VESA_MODE_INFORMATION_BUFFER_SIZE]; // VESA VBE视频模式信息
};

#ifdef KERNEL
#ifndef OSLDR
krnlapi extern struct syspage *syspage;
#endif
#endif

#define OSBASE                      0x80000000    //操作系统基地址
#define PAGE_TABLE_BASE             0x90000000    //页表基地址(页面目录表)
#define SYS_PAGE_BASE               0x90400000    //系统页基地址.包含TSS、GDT、IDT和引导参数
#define PAGE_FRAME_DATABASE_BASE    0x90800000    //包含所有物理内存页面信息的页面框架数据库
#define HANDLE_TABLE_BASE           0x91000000    //句柄表基地址(将句柄映射到内核对象的句柄表)
#define KERNEL_HEAP_BASE            0x92000000    //内核堆基地址

#define KMODSIZE    0x10000000
#define KHEAPSIZE   0x6E000000
#define HTABSIZE    0x01000000

#define SYSPAGE_ADDRESS (SYS_PAGE_BASE + 0 * PAGESIZE)    //页面大小（PAGESIZE）=4096
#define PAGEDIR_ADDRESS (SYS_PAGE_BASE + 1 * PAGESIZE)    //页目录表
#define INITTCB_ADDRESS (SYS_PAGE_BASE + 2 * PAGESIZE)    //
#define OSVMAP_ADDRESS  (SYS_PAGE_BASE + 4 * PAGESIZE)
#define KMODMAP_ADDRESS (SYS_PAGE_BASE + 5 * PAGESIZE)
#define VIDEO_BASE_ADDRESS (SYS_PAGE_BASE + 6 * PAGESIZE)

#define DMABUF_ADDRESS  (SYS_PAGE_BASE + 16 * PAGESIZE)  // 64K
#define INITRD_ADDRESS  (SYS_PAGE_BASE + 32 * PAGESIZE)  // 512K

#define TSS_ESP0 (SYSPAGE_ADDRESS + 4)

#define USERSPACE(addr) ((unsigned long)(addr) < OSBASE)
#define KERNELSPACE(addr) ((unsigned long)(addr) >= OSBASE)

#endif
